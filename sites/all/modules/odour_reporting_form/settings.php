<?php
   define('ODOUR_REPORTING_FORM_TO_EMAIL_ADDRESS', 'CODMP@dwer.wa.gov.au');
   define('ODOUR_REPORTING_FORM_CC_EMAIL_ADDRESS', 'helena.moneta@dwer.wa.gov.au,philippe.najean@dwer.wa.gov.au');
   define('ODOUR_REPORTING_FORM_EMAIL_SUBJECT', 'Community Odour Monitoring Report Form Submission');
   define('ODOUR_REPORTING_FORM_EMAIL_KEY', 'information');
   define('ODOUR_REPORTING_FORM_MODULE_NAME', 'odour_reporting_form');
   define('ODOUR_REPORTING_FORM_MODULE_FORM_ELEMENT_PREFIX', 'odour_reporting_form_');
   
   global $emailElements;
   
   $emailElements = array(
      'reporter_id' => array(
         'label' => 'Reporter Id',
	 'type' => 'text'     
      ),
      'start_datetime' => array(
         'label' => 'Start Datetime',
         'csv_label' => ['Start Date', 'Start Time'], 
         'type' => 'text',
         'split_datetime' => true
      ),
      'end_datetime' => array(
         'label' => 'End Datetime',
         'csv_label' => ['End Date', 'End Time'],
         'type' => 'text',
         'split_datetime' => true
      ),
      'location' => array(
         'label' => 'Location',
         'type' => 'text'
      ),
      'sky' => array(
         'label' => 'Sky Condition',
         'type' => 'select'
      ),
      'rain' => array(
         'label' => 'Rain Condition',
         'type' => 'select'
      ),
      'wind_speed' => array(
         'label' => 'Wind Speed',
         'type' => 'select'
      ),
      'wind_direction' => array(
         'label' => 'Wind Direction',
         'type' => 'select'
      ),
      'health_condition' => array(
         'label' => 'Existing Health Condition',
         'type' => 'select'
      ),
      'health_condition_description' => array(
         'label' => 'Existing Health Condition Description',
         'type' => 'text'
      ),
      'odour_intensity' => array(
         'label' => 'Odour Intensity',
         'type' => 'text'
      ),
      'odour_frequency' => array(
         'label' => 'Odour Frequency',
         'type' => 'select'
      ),
      'odour_frequency_description' => array(
         'label' => 'Odour Frequency Description',
         'type' => 'text'
      ),
      'odour_characteristic' => array(
         'label' => 'Characteristic of Odour',
         'type' => 'checkboxes'
      ),
      'odour_characteristic_other' => array(
         'label' => 'Other',
         'type' => 'text'
      ),
      'perceived_source_of_odour' => array(
         'label' => 'Perceived Source Of Odour',
         'type' => 'text'
      ),
      'odour_annoyace_level' => array(
         'label' => 'Annoyance Level of Odour',
         'type' => 'select'
      ),
      'order_annoyance_level_details' => array(
         'label' => 'Detials Of Annoyance Level of Odour',
         'type' => 'select'
      ),
      'impact_on_household' => array(
         'label' => 'Impact on Household / Family',
         'type' => 'textarea'
      ),
      'health_impact' => array(
         'label' => 'Impact on Health',
         'type' => 'textarea'
      ),
      'comments' => array(
         'label' => 'Additionl Comments',
         'type' => 'textarea'
      )
   );
?>