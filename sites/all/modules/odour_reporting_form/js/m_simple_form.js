$jq(document).ready(function () { 
   $jq('#odour_reporting_form_start_datetime').datetimepicker({
      step: 5,
      onChangeDateTime: function(dp, $input) {
         if($input.val())
         {
            var endDateTime = $jq('#odour_reporting_form_end_datetime').val();
            if(endDateTime)
	    {
	       var startDateTime = new Date($input.val());
               endDateTime = new Date(endDateTime);
               
               var startDate = startDateTime.getDate();
               var startTime = startDateTime.getTime();
               var endDate = endDateTime.getDate();
               var endTime = endDateTime.getTime();
	       
    	       if(startDateTime > endDateTime)	         
               if((startDate == endDate && startTime > endTime) || (startDate > endDate))
                  $jq('#odour_reporting_form_end_datetime').val('');                     
	    }
	 }
      },
      onShow: function() {
         var endDateTime = $jq('#odour_reporting_form_end_datetime').val();
         if(endDateTime)
            endDateTime = new Date(endDateTime);
         else
            endDateTime = false;

         this.setOptions({
            maxDate: endDateTime
         });   
      }
   });
   
   
   $jq('#odour_reporting_form_end_datetime').datetimepicker({
      step: 5,
      onChangeDateTime: function(dp, $input) {
         if($input.val())
         {
            var startDateTime = $jq('#odour_reporting_form_start_datetime').val();
            if(startDateTime)
	    {
               startDateTime = new Date(startDateTime);
	       var endDateTime = new Date($input.val());
               
               var startDate = startDateTime.getDate();
               var startTime = startDateTime.getTime();
               var endDate = endDateTime.getDate();
               var endTime = endDateTime.getTime();
	       
               if(startDateTime > endDateTime)
                  $jq('#odour_reporting_form_start_datetime').val('');                     
	    }
	 }
      },
      onShow: function() {
         var startDateTime = $jq('#odour_reporting_form_start_datetime').val();
         if(startDateTime)
            startDateTime = new Date(startDateTime);
         else
            startDateTime = false;

         this.setOptions({
            minDate: startDateTime
         });  
      }
   });

});