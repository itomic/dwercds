<div class="node-add-wrapper clear-block">
  <div class="node-column-sidebar">
    <?php if(isset($sidebar)): ?>
      <?php print render($sidebar); ?>
    <?php endif; ?>
  </div>
  <div class="node-column-main">
    <?php if(isset($form)): ?>
      <form name = "" id = "m_simple_form_main_form" method = "post">
         <?php echo drupal_render_children($form); ?>
      </form>   
    <?php endif; ?>
   
    <?php if(isset($buttons)): ?>
      <div class="node-buttons">
        <?php print render($buttons); ?>
    </div>
  <?php endif; ?>
  </div>
  <div class="clear"></div>
</div>
