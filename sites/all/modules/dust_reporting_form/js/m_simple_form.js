
function initDatetimePickers() {
   $jq('#dust_reporting_form_start_datetime').datetimepicker({
      timepicker: false,
      format: 'Y/m/d'    
   });

   $jq('#dust_reporting_form_end_datetime').datetimepicker({
      timepicker: false,
      format: 'Y/m/d'
   });
}

function handleDustColorChange() {
   $jq('#dust_reporting_form_colour_other').attr('disabled', 'disabled');

   $jq('#dust_reporting_form_colour').on('change', function() {
      if($jq(this).val() == 'other')
         $jq('#dust_reporting_form_colour_other').removeAttr('disabled');
      else
         $jq('#dust_reporting_form_colour_other').val('').attr('disabled', 'disabled');
   });
}

function handleWindDirectionChange() {
   $jq('#dust_reporting_form_wind_direction').on('change', function() {
      if($jq(this).val() != 'No Selection')
         $jq('#dust_reporting_form_wind_direction_unsure').prop('checked', false);   
   });

   $jq('#dust_reporting_form_wind_direction_unsure').on('change', function() {
      if($jq(this).is(':checked'))
         $jq('#dust_reporting_form_wind_direction').val('No Selection');   
   });
}

function handlePeriodOfTheDayChange(excludedIdArray) {
   $jq('.period_of_the_day_checkbox.form-checkbox').each(function(index, item) {
      if(excludedIdArray.indexOf($jq(item).attr('id')) == -1)
         $jq(item).prop('checked', false);
   });
}

$jq(document).ready(function () {
   initDatetimePickers();
   handleDustColorChange();
   handleWindDirectionChange();

   $jq('.period_of_the_day_checkbox.form-checkbox').on('change', function() {
      if($jq(this).is(':checked'))
      {
         var podId = $jq(this).attr('id');
         var pod = $jq(this).val().toLowerCase();

         if(pod == 'unsure' || pod == 'night')
            handlePeriodOfTheDayChange([podId]);
         else
         {
            if(pod == 'morning' || pod == 'afternoon')
               handlePeriodOfTheDayChange(['edit-dust-reporting-form-period-of-the-day-afternoon', 'edit-dust-reporting-form-period-of-the-day-morning']);      
         }  
      }
   });

});

/*
function initDatetimePickers() {
   $jq('#dust_reporting_form_start_datetime').datetimepicker({
      step: 5,
      onChangeDateTime: function(dp, $input) {
         if($input.val())
         {
            if($jq('#dust_reporting_form_start_datetime_unsure').is(':checked'))
               $jq('#dust_reporting_form_start_datetime_unsure').prop('checked', false);   
	 }
      }
      
      //onShow: function() {
      //   var endDateTime = $jq('#dust_reporting_form_end_datetime').val();
      //   if(endDateTime)
      //   {
      //      endDateTime = new Date(endDateTime);            
      //      this.setOptions({
      //         maxDate: endDateTime
      //      });
      //   }   
      //}
      
   });

   $jq('#dust_reporting_form_start_datetime_unsure').on('change', function(){
      if($jq(this).is(':checked'))
         $jq('#dust_reporting_form_start_datetime').val('');   
   });
}

function handleDurationHourMinute() {
   $jq('#dust_reporting_form_duration_hour').on('change', function() {
      if($jq(this).val() > 0)
         $jq('#dust_reporting_form_duration_unsure').prop('checked', false);   
   });
   
   $jq('#dust_reporting_form_duration_minute').on('change', function() {
      if($jq(this).val() > 0)
         $jq('#dust_reporting_form_duration_unsure').prop('checked', false);   
   });
   
   $jq('#dust_reporting_form_duration_unsure').on('change', function() {
      if($jq('#dust_reporting_form_duration_unsure').is(':checked'))
      {
         $jq('#dust_reporting_form_duration_hour').val('0');
         $jq('#dust_reporting_form_duration_minute').val('0');
      }  
   });
}

*/
