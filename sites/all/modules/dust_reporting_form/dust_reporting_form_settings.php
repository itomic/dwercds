<?php
   define('DUST_REPORTING_FORM_TO_EMAIL_ADDRESS', 'CODMP@dwer.wa.gov.au');
   define('DUST_REPORTING_FORM_CC_EMAIL_ADDRESS', 'philippe.najean@dwer.wa.gov.au,helena.moneta@dwer.wa.gov.au');
   define('DUST_REPORTING_FORM_EMAIL_SUBJECT', 'Community Dust Monitoring Report Form Submission');
   define('DUST_REPORTING_FORM_EMAIL_KEY', 'information');
   define('DUST_REPORTING_FORM_MODULE_NAME', 'dust_reporting_form');
   define('DUST_REPORTING_FORM_MODULE_FORM_ELEMENT_PREFIX', 'dust_reporting_form_');
   
   global $dust_reporting_form_emailElements;
   
   $dust_reporting_form_emailElements = array(
      'reporter_id' => array(
         'label' => 'Reporter Id',
	      'type' => 'text'     
      ),
      'start_datetime' => array(
         'label' => 'Start Date',
         'type' => 'text',
         'csv_label' => 'Start Date'
      ),
      'end_datetime' => array(
         'label' => 'End Date',
         'type' => 'text',
         'csv_label' => 'End Date'
      ),
      'period_of_the_day' => array(
         'label' => 'Period of the day', 
         'csv_label' => 'POD',
         'type' => 'checkboxes' 
      ),
      'wind_direction' => array(
         'label' => 'Wind direction',
         'csv_label' => 'WD',
         'type' => 'select'     
      ),
      'wind_direction_unsure' => array(
         'label' => 'Unsure about Wind direction',
         'csv_label' => 'U_WD',
         'type' => 'checkbox'     
      ),
      'period_location_comment' => array(
         'label' => 'Comment for Period & Location', 
         'csv_label' => 'PL_Comment',
         'type' => 'textarea'  
      ),
      'location' => array(
         'label' => 'Location',
         'type' => 'text'
      ),
      'colour' => array(
         'label' => 'Colour',
         'type' => 'select'
      ),
      'colour_other' => array(
         'label' => 'If other please specify',
         'type' => 'text'
      ),
      'dust_looks' => array(
         'label' => 'How the dust looks',
         'csv_label' => 'Dust look', 
         'type' => 'select'
      ),
      'deposition_density' => array(
         'label' => 'Deposition Density',
         'type' => 'select'
      ),
      'perceived_source_of_dust' => array(
         'label' => 'Perceived Source Of Dust',
         'type' => 'textarea'
      ),
      'dust_annoyance_level' => array(
         'label' => 'Dust annoyance level',
         'type' => 'select'
      ),
      'dust_annoyance_level_details' => array(
         'label' => 'Details of annoyance level',
         'type' => 'textarea'
      ),
      'impacts_on_household' => array(
         'label' => 'Impacts on household / family',
         'type' => 'textarea'
      ),
      'health_impacts' => array(
         'label' => 'Health impacts',
         'type' => 'textarea'
      ),
      'comments' => array(
         'label' => 'Comments',
         'type' => 'textarea'
      )
   );
?>