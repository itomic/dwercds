<?php

   require_once drupal_get_path('module', 'dust_reporting_form').'/dust_reporting_form_settings.php';
   
   /**
    * hook_theme()
    */
   function dust_reporting_form_theme($existing, $type, $theme, $path) 
   {
      return array(
         'dust_form' => array(
            'render element' => 'element',
            'template' => 'dust_reporting_form--main_form'
            // this will set to module/theme path by default:
            //'path' => drupal_get_path('module', 'm_simple_form'),
          ),
          'access_restricted_page' => array(
             'render element' => 'element',
             'template' => 'dust_reporting_form--access_restricted'
          )
      );
   }
   
   /**
    * hook_menu()
    */
   function dust_reporting_form_menu()
   {  
      $menu = [];
      $menu['dust_report_form'] = array(
         'title' => 'Dust Reporting Form',
         'page callback' => 'dust_reporting_form_menu_callback',
         'access callback' => TRUE,
      );
      return $menu;
   }
   
   /**
    * page callback function for hook_menu
    */
   function dust_reporting_form_menu_callback()
   {
      if(isset($_SESSION['custom']['authenticated']) && $_SESSION['custom']['authenticated'])
      {
         $output = [];
         $output['form'] = drupal_get_form('dust_reporting_form_form');
         
         $modulePath = drupal_get_path('module', DUST_REPORTING_FORM_MODULE_NAME);	
         
         /// add datetimepicker  ///
         drupal_add_js($modulePath.'/js/jquery_datetime_picker/jquery.js');
         drupal_add_js($modulePath.'/js/jquery_datetime_picker/build/jquery.datetimepicker.full.min.js');
         drupal_add_css($modulePath.'/js/jquery_datetime_picker/jquery.datetimepicker.css');
         drupal_add_js('var $jq = jQuery.noConflict();', array('type' => 'inline'));
               
         drupal_add_js($modulePath.'/js/m_simple_form.js');
         drupal_add_css($modulePath.'/css/m_simple_form.css');
               
         return theme('dust_form', $output); 
      }
      
      return theme('access_restricted_page');
   }
   
   /**
    * THis helper function generats the select options based on the array provided
    * which can be used to generate the drop down
    */
   function _dust_reporting_form_generateSelectOptions(Array $options, $emptyOption = false)
   {
      $output = [];
      
      if($emptyOption)
         $output['No Selection'] = '- Select -';
         
      foreach($options as $option)
         $output[$option] = t($option);
         
      return $output;         
   }
   
   /**
    * This helper function generates the prefix for the form element 
    *
    */
   function _dust_reporting_form_generatePrefix($divContent)
   {
      return '<div style = "margin-bottom:0px; font-size:20px; font-weight:bold;">'.$divContent.'</div>';
   }
   
   function _dust_reporting_form_generateTextfield(&$form, Array $option)
   {
      $name = ((isset($option['name']) && trim($option['name']) !== '') ? trim($option['name']) : false);
      $id = ((isset($option['id']) && trim($option['id']) !== '') ? trim($option['id']) : false);
      $title = ((isset($option['title']) && trim($option['title']) !== '') ? trim($option['title']) : false);
      $required = ((isset($option['required']) && is_bool($option['required']) !== false) ? $option['required'] : false);
      $prefix = ((isset($option['prefix']) && trim($option['prefix']) !== '') ? trim($option['prefix']) : false);
      $suffix = ((isset($option['suffix']) && trim($option['suffix']) !== '') ? trim($option['suffix']) : false);
      $attributes = ((isset($option['attributes']) && is_array($option['attributes']) && count($option['attributes']) > 0) ? $option['attributes'] : false);
      
      if($name === false)
         throw new Exception('A name must be provided to create textfield form element');
         
      $id = (($id === false) ? $name : $id);
      
      $form[$name] = array(
         '#id' => $id,
         '#required' => $required,
         '#type' => 'textfield'
      );
      
      if($title !== false)
         $form[$name]['#title'] = $title;
      
      if($prefix !== false)
         $form[$name]['#prefix'] = $prefix;
         
      if($suffix !== false)
         $form[$name]['#suffix'] = $suffix; 
         
      if($attributes !== false)
         $form[$name]['#attributes'] = $attributes;   
         
      return true;     
   }
   
   function _dust_reporting_form_generateSelect(&$form, Array $option)
   {
      $name = ((isset($option['name']) && trim($option['name']) !== '') ? trim($option['name']) : false);
      $id = ((isset($option['id']) && trim($option['id']) !== '') ? trim($option['id']) : false);
      $title = ((isset($option['title']) && trim($option['title']) !== '') ? trim($option['title']) : false);
      $required = ((isset($option['required']) && is_bool($option['required']) !== false) ? $option['required'] : false);
      $prefix = ((isset($option['prefix']) && trim($option['prefix']) !== '') ? trim($option['prefix']) : false);
      $suffix = ((isset($option['suffix']) && trim($option['suffix']) !== '') ? trim($option['suffix']) : false);
      $options = ((isset($option['options']) && is_array($option['options']) && count($option['options']) > 0) ? $option['options'] : false);
      $attributes = ((isset($option['attributes']) && is_array($option['attributes']) && count($option['attributes']) > 0) ? $option['attributes'] : false);
      
      if($name === false)
         throw new Exception('A name must be provided to create select form element');
         
      if($options === false)
         throw new Exception('A list of options must be provided to create select from element'); 
         
      $id = (($id === false) ? $name : $id);
      
      $form[$name] = array(
         '#id' => $id,
         '#required' => $required,
         '#type' => 'select'
      );
      
      if($title !== false)
         $form[$name]['#title'] = $title;
      
      if($prefix !== false)
         $form[$name]['#prefix'] = $prefix;
         
      if($suffix !== false)
         $form[$name]['#suffix'] = $suffix;
         
      if($attributes !== false)
         $form[$name]['#attributes'] = $attributes;
         
      return true;           
   }
   
   function _dust_reporting_form_constructEmailContent($formData)
   {
      global $dust_reporting_form_emailElements;
      
      $emailBody = '
         <style>
            .row {
               height:20px;
               margin-bottom:5px;
            }
         </style>
         <div style = "font-family:arial; font-size:13px;">
      ';
      
      foreach($dust_reporting_form_emailElements as $key => $value)
      {
         $newKey = DUST_REPORTING_FORM_MODULE_FORM_ELEMENT_PREFIX.$key;
         
         if(isset($formData[$newKey]))
         { 
            if(isset($value['type']))
            {
               if(in_array($value['type'], array('text', 'select', 'textarea')))
               {
                  if($value['type'] == 'select' || $value['type'] == 'text')
                     $emailBody .= '<div class = "row"><b>'.$value['label'].":</b> ".trim($formData[$newKey]).'</div>';
                  else 
                  {
                     $emailBody .= '<div><b>'.$value['label'].':</b></div>';
                     $emailBody .= '<div class = "row">'.trim($formData[$newKey]).'</div>';
                  }      
               }   
               else if(in_array($value['type'], array('checkboxes')))
               {
                  $tmp = [];
                  foreach($formData[$newKey] as $k => $v)
                  {
                     if($v !== 0 && trim($v) !== '')
                        $tmp[] = trim($v);
                  }
               
                  if(count($tmp) > 0)
                     $emailBody .= '<div class = "row"><b>'.$value['label'].':</b> '.implode(", ", $tmp).'</div>';   
               }
               else if(in_array($value['type'], array('checkbox')))
               {
                  if(trim($formData[$newKey]) === '1')
                     $emailBody .= '<div class = "row"><b>'.$value['label'].'</b>: Y</div>';
                  else
                     $emailBody .= '<div class = "row"><b>'.$value['label'].'</b>: N</div>';   
               }
               else
                  $emailBody .= '<div class = "row"><b>'.$value['label'].':</b> '.$formData[$newKey].'</div>';
            }
            else
               $emailBody .= '<div class = "row"><b>'.$value['label'].':</b> '.$formData[$newKey].'</div>';
         }
      }
      
      $emailBody .= "<div style = 'margin-top:20px; font-style:italic;'>Form submitted at ".date('Y-m-d H:i')."</div>";
      
      return $emailBody;
   }
   
   function _dust_reporting_form_constructCSV($formData)
   {
      global $dust_reporting_form_emailElements;
      
      $csvFileFullPath = '/tmp/'.date('YmdHis').rand().'.csv';
      $fp = fopen($csvFileFullPath, 'w');
      if(!$fp)
         return false;
         
      $header = $data = [];   
        
      foreach($dust_reporting_form_emailElements as $key => $value)
      {
         $newKey = DUST_REPORTING_FORM_MODULE_FORM_ELEMENT_PREFIX.$key;
         
         if(isset($formData[$newKey]))
         { 
            $lblKey = (isset($value['csv_label']) ? 'csv_label' : 'label');
            if(is_array($value[$lblKey]))
            {
               foreach($value[$lblKey] as $lbl)
                  $header[] = $lbl;
            }
            else
               $header[] = $value[$lblKey];
            
            if(isset($value['type']))
            {
               if(in_array($value['type'], array('text', 'select', 'textarea')))
               {
                  if(isset($value['split_datetime']) && $value['split_datetime'])
                  {
                     $dt = $tm = '';
                     if(trim($formData[$newKey]) !== '')
                        list($dt, $tm) = explode(" ", $formData[$newKey]);
                     
                     $data[] = $dt;
                     $data[] = $tm;  
                  }
                  else
                     $data[] = trim($formData[$newKey]);
               }   
               else if(in_array($value['type'], array('checkboxes')))
               {
                  $tmp = [];
                  foreach($formData[$newKey] as $k => $v)
                  {
                     if($v !== 0 && trim($v) !== '')
                        $tmp[] = trim($v);
                  }
               
                  if(count($tmp) > 0)
                  {
                     if($key == 'period_of_the_day')
                     {
                        if(in_array('Morning', $tmp) && in_array('Afternoon', $tmp))
                           $tmp = ['Daytime'];
                     }      
                        
                     $data[] = implode(", ", $tmp);
                  }   
                  else
                     $data[] = '';
               }
               else if(in_array($value['type'], array('checkbox')))
               {
                  if(trim($formData[$newKey]) === '1')
                     $data[] = 'Y';
                  else
                     $data[] = 'N';   
               }
               else
                  $data[] = $formData[$newKey];
            }
            else
               $data[] = $formData[$newKey];
         }
      }
      
      fputcsv($fp, $header);
      fputcsv($fp, $data);
      fclose($fp);
      
      return $csvFileFullPath;
      
   }
   
   function _dust_reporting_form_emailSubmittedForm($formData)
   {
      $email_content = _dust_reporting_form_constructEmailContent($formData);
      $csvFilePath = _dust_reporting_form_constructCSV($formData);
      
      $params = array(
         'body' => array($email_content),
         'subject' => DUST_REPORTING_FORM_EMAIL_SUBJECT
      );
      
      if($csvFilePath !== false)
      {  
         $params['attachments'] = array(
            'filecontent' => file_get_contents($csvFilePath),
            'filename' => 'attachment.csv',
            //'filemime' => 'text/csv'
         );
         
         @unlink($csvFilePath); 
      }
      
      $key = DUST_REPORTING_FORM_EMAIL_KEY;
      $to = DUST_REPORTING_FORM_TO_EMAIL_ADDRESS;
      $mail = drupal_mail(DUST_REPORTING_FORM_MODULE_NAME, $key, $to, language_default(), $params);
   }
   
   /* hook_mail_alter() */
   function dust_reporting_form_mail_alter(&$message)
   {
      //$message['headers']['Content-Type'] = 'text/html; charset=UTF-8; format=flowed';
   }
   
   /* hook_mail() */
   function dust_reporting_form_mail($key, &$message, $params) 
   {
      switch ($key) {
         case DUST_REPORTING_FORM_EMAIL_KEY:
            $message['subject'] = $params['subject'];
            $message['body'] = $params['body'];
            //$message['from'] = 'Admin - DWER';

            if(DUST_REPORTING_FORM_CC_EMAIL_ADDRESS != '')
               $message['headers']['Cc'] = DUST_REPORTING_FORM_CC_EMAIL_ADDRESS ;
               
            if(isset($params['attachments']))
            {
               $message['params']['attachments'][] = $params['attachments']; 
               //$message['headers']["Content-Type"] = "multipart/mixed; boundary=\"".md5("randomstring12121212")."\"" ; 
            }
            break;
      }
   }

   /* hook_form() */
   function dust_reporting_form_form($form, &$form_state) 
   {
      //if(isset($form_state['storage']) && count($form_state['storage']) > 0)
      if(isset($_SESSION['dust_reporting_form']['submitted_data']))
      {
         _dust_reporting_form_emailSubmittedForm($_SESSION['dust_reporting_form']['submitted_data']);
         drupal_set_message("Thank you for submitting the report");
         unset($_SESSION['dust_reporting_form']['submitted_data']);
      }   

      $elementPrefix = DUST_REPORTING_FORM_MODULE_FORM_ELEMENT_PREFIX;
      
      $form[$elementPrefix.'reporter_id'] = array(
         '#type' => 'textfield',
         '#id' => $elementPrefix.'reporter_id',
         '#title' => 'Reporter ID',
         '#required' => TRUE,
         '#prefix' => '<div><h2>Community dust reporting form</h2><br/><span style = "color:red;">*</span><span> : these fields are compulsory</span></div><div id= "reporter_id_holder">',
         '#suffix' => '</div>'
      );
      
      _generateTextfield($form, array(
         'name' => $elementPrefix.'start_datetime',
         'title' => 'Date the event started',
         'prefix' => _generatePrefix('1. Period and location of the event').'<div style = "margin-top:5px;">When have you noticed the occurrence of dust</div><div class = "m_simple_form_element_group">',
         'required' => true,
         'attributes' => array('class' => array('textbox_large'), 'readonly' => 'readonly'),
         'suffix' => ''
      ));
      
      _generateTextfield($form, array(
         'name' => $elementPrefix.'end_datetime',
         'title' => 'Date the event finished',
         'prefix' => '<div style = "float:left; margin-left: 10px; margin-right:40px; padding-top: 46px;">and</div>',
         'required' => true,
         'attributes' => array('class' => array('textbox_large'), 'readonly' => 'readonly'),
         'suffix' => '</div>'
      ));
      
      $periodOfTheDayOptions = array(
         'Morning' => 'Morning', 
         'Afternoon' => 'Afternoon',
         'Night' => 'Night',
         'Unsure' => 'Unsure'
      );
      
      $form[$elementPrefix.'period_of_the_day'] = array(
         '#id' => $elementPrefix.'period_of_the_day',
         '#type' => 'checkboxes',
         '#options' => $periodOfTheDayOptions,
         '#title' => 'Period of the day',
         '#attributes' => array('class' => array('period_of_the_day_checkbox')),
         '#prefix' => '<div style = "margin-top:10px;">Please provide additional information (if you think you know):</div>',
         '#required' => TRUE
      );
      
      $windDirectionOptions = ['North', 'North-East', 'East', 'South-East', 'South', 'South-West', 'West', 'North-West'];
      $form[$elementPrefix.'wind_direction'] = array(
         '#title' => 'Prevailing wind direction – please indicate<span class = "ashterisk">*</span>',
         '#id' => $elementPrefix.'wind_direction',
         '#type' => 'select',
         '#required' => FALSE,
         '#options' => _dust_reporting_form_generateSelectOptions($windDirectionOptions, true),
         '#prefix' => '<div class = "m_simple_form_element_group">'
      );
      
      $form[$elementPrefix.'wind_direction_unsure'] = array(
         '#id' => $elementPrefix.'wind_direction_unsure',
         '#type' => 'checkbox', 
         '#title' => 'Unsure',
         '#suffix' => '</div>'
      );
      
      $form[$elementPrefix.'period_location_comment'] = array(
         '#id' => $elementPrefix.'period_location_comment',
         '#type' => 'textarea',
         '#required' => FALSE,
         '#title' => 'Any other comments that you think are relevant'
      );
      
     _generateTextfield($form, array(
        'required' => true,
        'name' => $elementPrefix.'location',
        'title' => 'Location',
        'attributes' => array('class' => array('textbox_large')),
        //'prefix' => '<div style = "margin-top:5px;">&nbsp;</div>',
        'suffix' => '<div">(If unavailable, please state closest crossroads)</div><hr/>'
     ));

      $dustColourOptions = ['black', 'dark', 'grey', 'red', 'yellow', 'white', 'other'];
      $form[$elementPrefix.'colour'] = array(
         '#title' => 'Colour',
         '#id' => $elementPrefix.'colour',
         '#type' => 'select',
         '#required' => TRUE,
         '#options' => _dust_reporting_form_generateSelectOptions($dustColourOptions),
         '#prefix' => _dust_reporting_form_generatePrefix('2. Dust characteristic')            
      );

      $form[$elementPrefix.'colour_other'] = array(
         '#id' => $elementPrefix.'colour_other',
         '#title' => 'if other please specify',
         '#type' => 'textfield',
         '#requried' => FALSE
      );

      $dustLookOptions = array('Coarse', 'Fine');
      $form[$elementPrefix.'dust_looks'] = array(
         '#title' => 'How the dust looks',
         '#id' => $elementPrefix.'dust_looks',
         '#type' => 'select',
         '#required' => TRUE,
         '#options' => _dust_reporting_form_generateSelectOptions($dustLookOptions)
      );
      
      $depositionDensityGuideImage = drupal_get_path('module', DUST_REPORTING_FORM_MODULE_NAME).'/assets/ddg.jpg';
      $depositionDensityOptions = array('Low density', 'Medium density', 'High density');
      
      $form[$elementPrefix.'deposition_density'] = array(
         '#title' => 'Deposition Density',
         '#id' => $elementPrefix.'deposition_density',
         '#type' => 'select',
         '#required' => TRUE,
         '#options' => _dust_reporting_form_generateSelectOptions($depositionDensityOptions),
         '#suffix' => '
            <div>
               <h4>Deposition density guide</h4>
               <div><b>Low Density:</b> Dust particles covered less than 20% of the surface area.</div>
               <div><b>Medium Density:</b> Dust particles covered 20 to 50% of the surface area.</div>
               <div><b>High Density:</b> Dust particles covered more than 50% of the surface area.</div>
               <div>The following diagrams are a general guide to assist in determining how much dust fall out has occured</div>
            </div>
            <div class = "row">
               <img style = "width:50%;" src = "'.$depositionDensityGuideImage.'" alt = "Dust Deposition Guide" />
            </div>
            <hr/>
         '
      );

      $prefixHTML = _dust_reporting_form_generatePrefix('3. Perceived source of dust').'<div style = "margin-top:5px;">If you think you know, please state where you think the dust was emitted from.</div>';	
      $form[$elementPrefix.'perceived_source_of_dust'] = array(
         '#id' => $elementPrefix.'perceived_source_of_dust',
         //'#title' => 'Where do you think the dust was emitted from:',
         '#type' => 'textarea',
         '#required' => FALSE,
         '#prefix' => $prefixHTML,
         '#suffix' => '<hr/>' 
      );
      
      $dustAnnoyanceLevelOptions = ['Not Annoyed', 'Annoyed', 'Very Annoyed'];
      $form[$elementPrefix.'dust_annoyance_level'] = array(
         '#id' => $elementPrefix.'dust_annoyance_level',
         '#type' => 'select',
         //'#title' => '',
         '#options' => _dust_reporting_form_generateSelectOptions($dustAnnoyanceLevelOptions),
         '#required' => TRUE,
         '#prefix' => _dust_reporting_form_generatePrefix('4. Dust annoyance level<span style = "color:red; font-size:15px;">*</span>')
      );

      $form[$elementPrefix.'dust_annoyance_level_details'] = array(
         '#id' => $elementPrefix.'dust_annoyance_level_details',
         '#title' => 'Please provide details',
         '#type' => 'textarea',
         '#suffix' => '<hr/>'
      );
      
      $form[$elementPrefix.'impacts_on_household'] = array(
         '#id' => $elementPrefix.'impacts_on_household',
         '#title' => 'Please provide details',
         '#type' => 'textarea',
         '#suffix' => '<hr/>',
         '#prefix' => _dust_reporting_form_generatePrefix('5. Impacts on your household / family')
      );
      
      $form[$elementPrefix.'health_impacts'] = array(
         '#id' => $elementPrefix.'health_impacts',
         '#title' => 'Please provide details',
         '#type' => 'textarea',
         '#suffix' => '<hr/>',
         '#prefix' => _dust_reporting_form_generatePrefix('6. Health impacts')
      );
      
      $form[$elementPrefix.'comments'] = array(
         '#id' => $elementPrefix.'comments',
         '#title' => '',
         '#type' => 'textarea',
         '#suffix' => '<hr/>',
         '#prefix' => _dust_reporting_form_generatePrefix('Comments')
      );
     
      /*      
      $mapHTML = '
      	<style>
      		.embed-container {position: relative; padding-bottom: 80%; height: 0; max-width: 100%;} 
	      	.embed-container iframe, .embed-container object, 
      		.embed-container iframe{position: absolute; top: 0; left: 0; width: 100%; height: 100%;} small{position: absolute; z-index: 40; bottom: 0; margin-bottom: -15px;}
      	</style>
	<div class="embed-container">
		<iframe width="500" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" title="TEst" 
			src="//dow.maps.arcgis.com/apps/Embed/index.html?webmap=b3eee3774f0f44319df17f8968cf4959&extent=115.8174,-32.0035,115.9442,-31.9442&zoom=true&previewImage=false&scale=true&disable_scroll=true&theme=light">
		</iframe>
	</div>';
		
      $html = '<b>Map</b>';
      $html .= '<div id = "map">'.$mapHTML.'</div><br/>';
      $form['mymarkup'] = array(
         '#markup' => $html,
      );
      */
      
      $form['submit_button'] = array(
         '#type' => 'submit',
         '#value' => t('Submit'),
      );
      
      $form['#validate'][] = 'dust_reporting_form_validate';
      $form['#submit'][] = 'dust_reporting_form_form_submit';
      
      return $form;
   }

   function dust_reporting_form_validate($form, &$form_state) 
   {
      $prefix = DUST_REPORTING_FORM_MODULE_FORM_ELEMENT_PREFIX;
      
      //echo '<pre>';
      //var_dump($form_state['values']);
      //echo '</pre>';
      
      $reporterId = trim($form_state['values'][$prefix.'reporter_id']);
      if(!is_numeric($reporterId))
         form_set_error($prefix.'reporter_id', 'Check your reporter ID. It must be a number');
      
      $startDatetime = trim($form_state['values'][$prefix.'start_datetime']);
      $endDatetime = trim($form_state['values'][$prefix.'end_datetime']);
      
      if(strtotime($startDatetime) === false || strtotime($endDatetime) === false)
      {
         if(strtotime($startDatetime) === false)
            form_set_error($prefix.'start_datetime', 'Start date must be a valid date');
      
         if(strtotime($endDatetime) === false)
            form_set_error($prefix.'end_datetime', 'End date must be a valid date');
      }      
      else
      {   
         if(strtotime($startDatetime) > strtotime($endDatetime))
            form_set_error($prefix.'start_datetime', 'Start date must be smaller than end date');
      }      
      
      if(trim($form_state['values'][$prefix.'wind_direction']) === 'No Selection' && 
        (!isset($form_state['values'][$prefix.'wind_direction_unsure']) || $form_state['values'][$prefix.'wind_direction_unsure'] === 0)
      )
      {
         form_set_error($prefix.'wind_direction', 'Prevailing wind direction must be provided');   
      }

      if(trim($form_state['values'][$prefix.'colour']) === 'other')
      {
         if(trim($form_state['values'][$prefix.'colour_other']) === '')
            form_set_error($prefix.'colour_other', 'Dust Colour must be provided');
      }
      
      if(trim($form_state['values'][$prefix.'dust_annoyance_level']) === '')
      {
      	 form_set_error($prefix.'dust_annoyance_level', 'Dust Annoyance Level must be provided');
      	 drupal_set_message('Dust Annoyance Level must be provided', 'error');
      }
   }

   function dust_reporting_form_form_submit($form, &$form_state) 
   {
      $_SESSION['dust_reporting_form']['submitted_data'] = $form_state['values'];
      //$form_state['rebuild'] = TRUE;
   }
