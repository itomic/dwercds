<?php

/**
 * @file
 */

/**
 * Style plugin.
 */
class porto_views_views_plugin_layouts extends views_plugin_style {

  /**
   * Set default options.
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['porto_views']['optionset'] = array('default' => ''); // Declare style settings so they can be exported properly.
	$options['porto_views']['style'] = array('default' => 'grid');
	$options['porto_views']['margin'] = array('default' => '1');

	return $options;
  }

  /**
   * Render the given style.
   */
  
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['porto_views'] = array(
    	'#type' => 'fieldset',
    	'#title' => t('porto'),
    );
    $form['porto_views']['optionset'] = array(
  		'#title' => t('Template'),
  		'#description' => t('Select a template for this view.'),
  		'#type' => 'select',
  		'#options' => array(
	  		'blog_posts' => t('Blog Posts'),
  			'blog_timeline' => t('Blog Timeline'),
  			// will add later 'blog_carousel' => t('Blog Carousel'),
  			'carousel' => t('Carousel'),
  			'portfolio_one' => t('Portfolio One Column'),
  			'portfolio_two' => t('Portfolio Two Columns'),
  			'portfolio_three' => t('Portfolio Three Columns'),
  			'portfolio_four' => t('Portfolio Four Columns'),
  			'portfolio_five' => t('Portfolio Five Columns'),
  			'portfolio_six' => t('Portfolio Six Columns'),
  			'portfolio_full' => t('Portfolio Fullwidth'),
  			'portfolio_list' => t('Portfolio Title and Description'),
  			'portfolio_lightbox' => t('Portfolio Lightbox'),
  			'portfolio_timeline' => t('Portfolio Timeline'),
  			'portfolio_carousel' => t('Portfolio Carousel'),
  			'portfolio_carousel_lightbox' => t('Portfolio Carousel + Lightbox'),
  			'portfolio_related_projects' => t('Portfolio Related Projects'),
  			'team' => t('Team'),
  			'categories' => t('Categories List'),
  			'portfolio_filter' => t('Portfolio Isotope Filters'),
  			'portfolio_filter_full' => t('Portfolio Isotope Filters (Full Width)'),
  			'shop' => t('Shop'),
  			'team_filter' => t('Team Isotope Filters')
  		),
  		'#default_value' => $this->options['porto_views']['optionset'],
    );
	$form['porto_views']['style'] = array(
        '#type' => 'select',
        '#title' => t('Style Options'),
        '#options' => array(
            'grid' => 'Grid',
            'masonry' => 'Masonry',      
          ),
        '#default_value' => $this->options['porto_views']['style'],
		'#states' => array (
          'visible' => array(
            'select[name="style_options[porto_views][optionset]"]' => array(
				array('value' => 'portfolio_one'),
				array('value' => 'portfolio_two'),
				array('value' => 'portfolio_three'),
				array('value' => 'portfolio_four'),
				array('value' => 'portfolio_five'),
				array('value' => 'portfolio_six'),
			)
          )
        )
    );
	$form['porto_views']['margin'] = array(
        '#type' => 'select',
        '#title' => t('Margin OR No Margin'),
        '#options' => array(
            '0' => 'No',
            '1' => 'Yes',      
          ),
        '#default_value' => $this->options['porto_views']['margin'],	
		'#states' => array (
          'visible' => array(
            'select[name="style_options[porto_views][optionset]"]' => array(
				array('value' => 'portfolio_one'),
				array('value' => 'portfolio_two'),
				array('value' => 'portfolio_three'),
				array('value' => 'portfolio_four'),
				array('value' => 'portfolio_five'),
				array('value' => 'portfolio_six'),
				array('value' => 'portfolio_full'),
			)
          )
        )
    );
  }
}
