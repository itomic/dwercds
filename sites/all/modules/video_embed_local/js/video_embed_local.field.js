(function (Drupal, $) {
  Drupal.behaviors.videoEmbedLocalField = {
    attach: function (context, settings) {

      function processFieldset() {
        var $fieldset = $(this);
        var $url_field = $fieldset.find('.video_embed_url').closest('.form-type-textfield');
        var $upload_field = $fieldset.find('.form-type-managed-file');
        $upload_field.find('.form-submit').mousedown(function() {
          $url_field.find(':input').val('');
        });
        var $tabs = $('<div class="video-embed-local-tabs"></div>');
        var $url_tab = $('<a href="#">' + Drupal.t('Enter URL') + '</a>').click(function(e) {
          e.preventDefault();
          $url_field.show();
          $upload_field.hide();
        });
        var $upload_tab = $('<a href="#">' + Drupal.t('Upload file') + '</a>').click(function(e) {
          e.preventDefault();
          $url_field.hide();
          $upload_field.show();
        });

        $tabs.append($url_tab);
        $tabs.append(' ' + Drupal.t('or') + ' ');
        $tabs.append($upload_tab);
        $fieldset.find('.video-embed-local-tabs').remove();
        $fieldset.prepend($tabs);
        $url_tab.click();
      }

      $(context).find('.video-embed-local-fieldset .fieldset-wrapper').each(processFieldset);
    }
  };
})(Drupal, jQuery);