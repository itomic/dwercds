<?php
   function _container_deposit_scheme_checkIfLoggedIn()
   {
      return ((isset($_SESSION[MODULE_NAME]['user']) && is_array($_SESSION[MODULE_NAME]['user'])) ? true : false);
   }
   
   function _container_deposit_scheme_getLoggedUser()
   {
      if(_checkIfLoggedIn())
         return $_SESSION[MODULE_NAME]['user'];
         
      return false;   
   }

?>