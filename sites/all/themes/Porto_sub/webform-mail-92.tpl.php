<?php

/**
 * @file
 * Customize the e-mails sent by Webform after successful submission.
 *
 * This file may be renamed "webform-mail-[nid].tpl.php" to target a
 * specific webform e-mail on your site. Or you can leave it
 * "webform-mail.tpl.php" to affect all webform e-mails on your site.
 *
 * Available variables:
 * - $node: The node object for this webform.
 * - $submission: The webform submission.
 * - $email: The entire e-mail configuration settings.
 * - $user: The current user submitting the form. Always the Anonymous user
 *   (uid 0) for confidential submissions.
 * - $ip_address: The IP address of the user submitting the form or '(unknown)'
 *   for confidential submissions.
 *
 * The $email['email'] variable can be used to send different e-mails to
 * different users when using the "default" e-mail template.
 */
?>

<?php
   //var_dump(variable_get('mail_system'));
   echo '<div><b>First Name:</b> '.$submission->data[1][0].'</div>';
   echo '<div><b>Last Name:</b> '.$submission->data[2][0].'</div>';
   echo '<div><b>Address:</b> '.(trim($submission->data[3][0]) !== '' ? 'Unit '.$submission->data[3][0].', ' : '').$submission->data[5][0].', '.$submission->data[6][0].', '.$submission->data[7][0].'</div>';
   echo '<div><b>Mobile:</b> '.$submission->data[9][0].'</div>';
   echo '<div><b>Email:</b> '.$submission->data[18][0].'</div>';
   
   if(isset($submission->data[12]['First_Choice']))
      echo '<div><b>1st Preference:</b> '.((trim($submission->data[12]['First_Choice']) !== '') ? str_replace('_', '-', $submission->data[12]['First_Choice']) : 'N/A').'</div>';
      
   if(isset($submission->data[12]['Second_Choice']))
      echo '<div><b>2nd Preference:</b> '.((trim($submission->data[12]['Second_Choice']) !== '') ? str_replace('_', '-', $submission->data[12]['Second_Choice']) : 'N/A').'</div>';
      
   if(trim($submission->data[15][0]) !== '')
      echo '<div>User has requested to contact him/her to organize a briefing session time</div>';

   //print $email['html'] = $html;     
?>
