<?php

$payment = theme_get_setting('payment');

$payment_link = theme_get_setting('payment_link');

$sitemap = theme_get_setting('sitemap');

$sitemap_link = theme_get_setting('sitemap_link');

$contact_us = theme_get_setting('contact_us');

$contact_us_link = theme_get_setting('contact_us_link');

$accessibility = theme_get_setting('accessibility');

$accessibility_link = theme_get_setting('accessibility_link');

?>



<header id="header" class="header-no-border-bottom" data-plugin-options='{"stickyEnabled": true, "stickyEnableOnBoxed": true, "stickyEnableOnMobile": true, "stickyStartAt": 120, "stickySetTop": "-150px", "stickyChangeLogo": false}'>

<div class="header-body">

<div class="header-container container">

    <div class="header-row">

        <div class="header-column">

            <?php if (isset($page['branding'])) : ?>

                <?php print render($page['branding']); ?>

            <?php endif; ?>

            <?php if ($logo): ?>

                <div class="header-logo">

                    <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">

                        <img alt="<?php print t('Home'); ?>" width="111" height="<?php print theme_get_setting('logo_height'); ?>" data-sticky-width="82" data-sticky-height="<?php print theme_get_setting('sticky_logo_height'); ?>" data-sticky-top="33" src="<?php print $logo; ?>">

                    </a>

                </div>

            <?php endif; ?>

            <?php if ($site_name || $site_slogan): ?>

                <div id="name-and-slogan"<?php if ($disable_site_name && $disable_site_slogan) { print ' class="hidden"'; } ?>>



                    <?php if ($site_name): ?>

                        <?php if ($title): ?>

                            <div id="site-name"<?php if ($disable_site_name) { print ' class="hidden"'; } ?>>

                                <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>

                            </div>

                        <?php else: ?>

                            <h1 id="site-name"<?php if ($disable_site_name) { print ' class="hidden"'; } ?>>

                                <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>

                            </h1>

                        <?php endif; ?>

                    <?php endif; ?>



                    <?php if ($site_slogan): ?>

                        <div id="site-slogan"<?php if ( ($disable_site_slogan ) ) { print ' class="hidden"'; } if ( (!$disable_site_slogan ) AND ($disable_site_name) ) { print ' class="slogan-no-name"'; } ?>>

                            <?php print $site_slogan; ?>

                        </div>

                    <?php endif; ?>



                </div> <!-- /#name-and-slogan -->

            <?php endif; ?>

        </div>

        <div class="header-column">

            <div class="row">

                <nav class="header-nav-top">

                    <ul class="nav nav-pills">

                        
                        <?php if(isset($payment)):?>
                            <li class="hidden-xs">
                                <a href="<?php print $payment_link;?>"><i class=""></i> <?php print $payment;?></a>
                            </li>
                        <?php endif;?>

                        <?php if(isset($accessibility)):?>
                            <li class="hidden-xs">
                                <a href="<?php print $accessibility_link;?>"><i class="fa fa-angle-right"></i> <?php print $accessibility;?></a>
                            </li>
                        <?php endif;?>

                        <?php if(isset($sitemap)):?>
                            <li class="hidden-xs">
                                <a href="<?php print $sitemap_link;?>"><i class="fa fa-angle-right"></i> <?php print $sitemap;?></a>
                            </li>
                        <?php endif;?>

                        <?php if(isset($contact_us)):?>
                        <li class="hidden-xs">
                            <a href="<?php print $contact_us_link;?>"><i class="fa fa-angle-right"></i> <?php print $contact_us;?></a>
                        </li>
                        <?php endif;?>

                        

                        <?php if(isset($phone_number)):?>
                        <li>
                            <span class="ws-nowrap"><i class="fa fa-phone"></i> <?php print $phone_number;?></span>
                        </li>
                        <?php endif;?>

                    </ul>

                </nav>

            </div>

            <div class="row mb-md">

                <?php if (isset($page['header_search'])) : ?>
                    <!--unhide search bar in mobile view-->
                    <!--<div class="header-search hidden-xs">-->
                    <div class="header-search">

                        <?php print render($page['header_search']); ?>

                    </div>

                <?php endif; ?>

            </div>
            <div class="row">

                <?php if (isset($page['wog_search'])) : ?>
                    <!--unhide wog in mobile view-->
                    <!--<div class="header-wog-search hidden-xs">-->
                    <div class="header-wog-search">
                    

                        <?php print render($page['wog_search']); ?>

                    </div>

                <?php endif; ?>

            </div>

        </div>

    </div>

</div>

<div class="header-container header-nav header-nav-bar">

<div class="container">

<button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main">

    <i class="fa fa-bars"></i>

</button>

    <?php print render($page['header_icons']); ?>

<div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1 collapse">

<nav>

    <?php print render($page['header_menu']); ?>

</nav>

</div>

</div>

</div>

</div>

</header>

